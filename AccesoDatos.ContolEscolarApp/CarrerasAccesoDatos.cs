﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ContolEscolarApp
{
    public class CarrerasAccesoDatos
    {
        Conexion _conexion;

        public CarrerasAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public DataSet ObtenerCarreras()
        {
            string consulta = string.Format("select * from Carrera");
            return _conexion.ObtenerDatos(consulta, "Carrera");
        }
    }
}
