﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;

namespace AccesoDatos.ContolEscolarApp
{
    public class BackupAccesoDatos
    {
        Conexion _conexion;
        public BackupAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }

        public bool CopiaSeguridadSql(string destino)
        {
            return _conexion.RespaldarSql(destino);
        }
    }
}
