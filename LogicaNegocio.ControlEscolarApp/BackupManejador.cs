﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ContolEscolarApp;
using System.IO.Compression;
using System.IO;
using System.Threading;


namespace LogicaNegocio.ControlEscolarApp
{
    public class BackupManejador
    {
        BackupAccesoDatos _backupAccesoDatos;

        public BackupManejador()
        {
            _backupAccesoDatos = new BackupAccesoDatos();
        }

        public bool CopiaSeguridadSql(string destino)
        {
            return _backupAccesoDatos.CopiaSeguridadSql(destino);
        }
        public bool CopiaSeguridadDatos(string sourcePath, string destino)
        {
            try
            {
                if (Directory.Exists(sourcePath))
                {
                    var rootDir = Path.GetFileName(Path.GetDirectoryName(sourcePath));
                    var directoriesNames = Directory.GetDirectories(sourcePath);
                    for (int i = 0; i < directoriesNames.Length; i++)
                    {
                        var files = Directory.GetFiles(directoriesNames[i]);
                        for (int j = 0; j < files.Length; j++)
                        {
                            var nameFile = Path.GetFileName(files[j]);
                            string folderName = Path.GetFileName(Path.GetDirectoryName(files[j]));

                            if (!Directory.Exists(Path.Combine(destino, rootDir)))
                                Directory.CreateDirectory(Path.Combine(destino, rootDir));
                            if (!Directory.Exists(Path.Combine(destino, rootDir, folderName)))
                                Directory.CreateDirectory(Path.Combine(destino, rootDir, folderName));

                            File.Copy(files[j], Path.Combine(destino, rootDir, folderName, nameFile));
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Comprimir(string origen, string path)
        {
            Thread thread = new Thread(t =>
            {
                DateTime date = DateTime.Now;
                ZipFile.CreateFromDirectory(origen, Path.Combine(path, string.Format("ControlEscolar{0}_{1}_{2}_{3}_{4}_{5}.zip", date.Day, date.Month, date.Year, date.Hour, date.Minute, date.Second)));
                Directory.Delete(origen, true);
            })
            { IsBackground = true };
            thread.Start();
        }
    }
}
