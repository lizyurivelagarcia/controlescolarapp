﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Data;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolarApp
{
    public class GruposManejador
    {
        GruposAccesoDatos _gruposAccesoDatos;

        public GruposManejador()
        {
            _gruposAccesoDatos = new GruposAccesoDatos();
        }

        public void Eliminar(int idGrupo)
        {
            _gruposAccesoDatos.Eliminar(idGrupo);
        }

        public void Guardar(Grupo grupo)
        {
            _gruposAccesoDatos.Guardar(grupo);
        }

        public DataSet ObtenerDatos(string filtro)
        {
            return _gruposAccesoDatos.ObtenerDatos(filtro);
        }

        public Tuple<bool, string> EsNombreValido(Grupo grupo)
        {
            string mensaje = "";
            bool valido = true;

            if (grupo.Nombre.Length == 0)
            {
                mensaje = "El nombre del grupo es necesario";
                valido = false;
            }
            else if (grupo.Nombre.Length > 2)
            {
                mensaje = "La longitud para nombre del grupo es máximo 50 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
    }
}
