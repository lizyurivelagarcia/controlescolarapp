﻿using System;
using System.Data;
using System.Collections.Generic;
using Entidades.ControlEscolarApp;
using AccesoDatos.ContolEscolarApp;
using System.Text.RegularExpressions;
using Extensions.ControlEscolar;
using System.IO;

namespace LogicaNegocio.ControlEscolarApp
{
    public class EscuelasManejador
    {
        private EscuelasAccesoaDatos _EscuelaAccesoaDatos;
        private RutaManager _rutasManager;

        public EscuelasManejador()
        {
            _EscuelaAccesoaDatos = new EscuelasAccesoaDatos();
        }
        public EscuelasManejador(RutaManager rutasManager)
        {
            _rutasManager = rutasManager;
        }

        public void Eliminar(int IdEscuela)
        {
            _EscuelaAccesoaDatos.Eliminar(IdEscuela);
        }

        public bool Guardar(Escuela escuela)
        {
            return _EscuelaAccesoaDatos.Guardar(escuela);
        }


        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool TelefonoValido(string telefono)
        {
            var regex = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            var match = regex.Match(telefono);
            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public bool SavePicture(string sourcePath, string targetPath, string nameFile)
        {
            return _EscuelaAccesoaDatos.SavePicture(sourcePath, targetPath, nameFile);
        }

        public DataSet ObtenerLista(string filtro)
        {
            var list = new DataSet();
            list = _EscuelaAccesoaDatos.ObtenerLista(filtro);
            return list;
        }

        public Tuple<bool, string> EsEscuelaValido(Escuela escuela)
        {
            string mensaje = "";
            bool valido = true;

            if (escuela.Nombre.Length == 0)
            {
                mensaje = "El nombre de la escuela es necesario";
                valido = false;
            }
            else if (!NombreValido(escuela.Nombre))
            {
                mensaje = "Escribe un fomato valido para el nombre";
                valido = false;
            }
            else if (escuela.Nombre.Length > 15)
            {
                mensaje = "La longitud para nombre de escuela es máximo 15 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }

        //public Tuple<bool, string> EsTelefonoValido(Escuela escuela)
        //{
        //    string mensaje = "";
        //    bool valido = true;

        //    if (Convert.ToString(escuela.Telefono).Length >= 15)
        //    {
        //        mensaje = "El telefono no puede ser mayor a 15 digitos";
        //        valido = false;
        //    }
        //    return Tuple.Create(valido, mensaje);
        //}

        public bool CargarLogo(string fileName)
        {
            var nombreArchivo = new FileInfo(fileName);

            if (nombreArchivo.Length > 5000000)
                return false;
            else 
                return true;
        }

        public void LimpiarDocumento(int idEscuela, string tipoDocumento)
        {
            string rutaRepositorio = "";
            string extencion = "";

            switch (tipoDocumento)
            {
                case "png":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    extencion = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _rutasManager.RutaRepositoriosLogos;
                    extencion = "*.png";
                    break;
            }

            string ruta = Path.Combine(rutaRepositorio, idEscuela.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, extencion);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length!=0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                    if (archivoAnterior.Exists)
                    {
                        archivoAnterior.Delete();
                    }
                }
            }
        }

        public string GetNombreLogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);
            return archivoNombre.Name;
        }

        public void GuardarLogo(string fileName, int idEscuela)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_rutasManager.RutaRepositoriosLogos, idEscuela.ToString());

                if (Directory.Exists(ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(ruta);
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                        }
                    }
                    else
                    {
                        archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
                else
                {
                    _rutasManager.CrearRepositorioLogosEscuela(idEscuela);
                    archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                }
            }
        }
    }
}
