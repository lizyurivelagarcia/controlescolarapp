﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;
using System.IO;
using Extensions.ControlEscolar;

namespace ControlEscolarApp
{
    public partial class EscuelasModal : Form
    {
        private OpenFileDialog _dialogCargarLogo;
        private RutaManager _rutasManager;
        EstadoManejador _EstadoManejador;
        MunicipiosManejador _municipiosManejador;
        private EscuelasManejador _escuelaManejador;
        private Escuela _escuela;
        private bool _isEnablebinding = false;
        private bool _isImageClear;
        public EscuelasModal()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutasManager = new RutaManager(Application.StartupPath);
            _escuelaManejador = new EscuelasManejador();
            _escuela = new Escuela();
            _EstadoManejador = new EstadoManejador();
            _municipiosManejador = new MunicipiosManejador();
            BindingEscuela();
            _isEnablebinding = true;
            btnModificar.Visible = false;
        }
        private void BindingEscuela()
        {
            if (_isEnablebinding)
            {
                _escuela.Nombre = txtNombre.Text;
                _escuela.Director = txtDirector.Text;
                //_escuela.Domicilio = txtDomicilio.Text;
                //if (txtTelefono.Text != "")
                //_escuela.Telefono = int.Parse(txtTelefono.Text);
                //_escuela.Numeroexterior = txtNumExterior.Text;
                //_escuela.E_mail = txtE_mail.Text;
                //_escuela.Estado = CmbEstado.SelectedValue.ToString();
                //_escuela.Municipio = CmbMunicipio.Text;
                //_escuela.Paginaweb = txtPaginaWeb.Text;
                _escuela.Logo = txtRuta.Text;
            }
        }
        private void BindingEscuelaReload()
        {

            txtNombre.Text = _escuela.Nombre;
            txtDirector.Text = _escuela.Director;
            //_escuela.Domicilio = txtDomicilio.Text;
            //_escuela.Numeroexterior = txtNumExterior.Text;
            //_escuela.Telefono = int.Parse(txtTelefono.Text);
            //_escuela.E_mail = txtE_mail.Text;
            //_escuela.Estado = CmbEstado.SelectedValue.ToString();
            //_escuela.Municipio = CmbMunicipio.Text;
            //_escuela.Paginaweb = txtPaginaWeb.Text;
            txtRuta.Text = _escuela.Logo;
        }
        public EscuelasModal(Escuela escuela)
        {
            InitializeComponent();
            _escuelaManejador = new EscuelasManejador();
            _dialogCargarLogo = new OpenFileDialog();
            _escuela = escuela;
            BindingEscuelaReload();
            _EstadoManejador = new EstadoManejador();
            _municipiosManejador = new MunicipiosManejador();
            _isEnablebinding = true;
            CargarLogo();
        }
        private void EscuelasModal_Load(object sender, EventArgs e)
        {

        }
        private bool ValidarEscuela()
        {
            var res = _escuelaManejador.EsEscuelaValido(_escuela);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;

        }
        //private bool ValidarTelefono()
        //{
        //    var res = _escuelaManejador.EsTelefonoValido(_escuela);
        //    if (!res.Item1)
        //    {
        //        MessageBox.Show(res.Item2);
        //    }
        //    return res.Item1;

        //}
        private bool Guardar()
        {
            return _escuelaManejador.Guardar(_escuela);
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            BindingEscuela();

            string nameFile = txtNombre.Text;
            string sourcePath = txtRuta.Text.Trim();
            string targetPath = (@"c:\Logo Escuela\" + nameFile);
            _escuela.Logo = targetPath + @"\" + nameFile + ".jpg";

            try
            {
                bool error = false;
                //if (!ValidarEscuela())
                //    error = true;
                //if (!ValidarTelefono())
                //    error = true;

                if (!error)
                {
                    if (Guardar())
                        _escuelaManejador.SavePicture(sourcePath, targetPath, nameFile);
                    this.Close();
                }
            }
            catch (Exception)
            {

            }

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de cancelar", "Cancelar", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                this.Close();
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            //pictureBox1.Image = Image.FromFile(@"C:\Users\arturo\Documents\BackGrounImage");
        }

        private void BtnAgregarImagen_Click(object sender, EventArgs e)
        {
            //CargarLogo();

            OpenFileDialog BuscarImagen = new OpenFileDialog(); BuscarImagen.Filter = "Archivos de Imagen|*.jpg";
            BuscarImagen.FileName = "";
            BuscarImagen.Title = "Titulo del Dialogo";
            BuscarImagen.InitialDirectory = "C:\\"; BuscarImagen.FileName = this.txtRuta.Text;
            if (BuscarImagen.ShowDialog() == DialogResult.OK)
            {
                this.txtRuta.Text = BuscarImagen.FileName; String Direccion = BuscarImagen.FileName; this.pictureBox1.ImageLocation = Direccion;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void CargarLogo()
        {
            pictureBox1.ImageLocation = @"c:\Logo Escuela\"+_escuela.Nombre+@"\"+_escuela.Nombre+".jpg";
        }

        private void BtnEliminaI_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de eliminar el logo. Los cambios se guardaran al final de la edicion", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                EliminarLogo();
            }
            this.Close();
        }

        private void EliminarLogo()
        {
            //pictureBox1.ImageLocation == null;
            //_isImagenClear == null;
        }

        private void txtRuta_TextChanged(object sender, EventArgs e)
        {
            BindingEscuela();
        }

        private void LoadEntity()
        {
            txtNombre.Text = _escuela.Nombre;
            txtDirector.Text = _escuela.Director;
            pictureBox1.ImageLocation = null;

            if (string.IsNullOrEmpty(_escuela.Logo) && string.IsNullOrEmpty(_dialogCargarLogo.FileName))
            {
                pictureBox1.ImageLocation = _rutasManager.RutaLogoEscuela(_escuela);
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void BindEntity()
        {
            if (_isEnablebinding)
            {
                _escuela.Nombre = txtNombre.Text;
                _escuela.Director = txtDirector.Text;
            }
        }

        private void txtDirector_TextChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void Save()
        {
            try
            {
                if (pictureBox1.Image != null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);

                        if (!string.IsNullOrEmpty(_escuela.Logo))
                        {
                            _escuelaManejador.GuardarLogo(_dialogCargarLogo.FileName, 1);
                            _dialogCargarLogo.Dispose();
                        }
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
