﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolarApp;
using Entidades.ControlEscolarApp;

namespace ControlEscolarApp
{
    public partial class GruposModal : Form
    {
        private GruposManejador _gruposManejador;
        private Grupo _grupo;
        private CarrerasManejador _carrerasManejador;
        private bool modify;
        private object safdasfeasfdafs;

        public GruposModal(Grupo grupo)
        {
            InitializeComponent();
            _gruposManejador = new GruposManejador();
            _grupo = new Grupo();
            _carrerasManejador = new CarrerasManejador();

            if (grupo == null)
            {
                modify = false;
                btnListaAlumnos.Visible = false;
            }
            else
            {
                _grupo = grupo;
                BuildEditModal();
                modify = true;
                btnListaAlumnos.Visible = true;
            }
        }

        private void BuildGrupo()
        {
            _grupo.Nombre = txtNombreGrupo.Text;
            _grupo.Carrera = cmbCarrera.SelectedValue.ToString();
            if (!modify)
            {
                _grupo.Id = "";
            }
        }

        private void BuildEditModal()
        {
            txtNombreGrupo.Text = _grupo.Nombre;
            cmbCarrera.Text = _grupo.Carrera;
        }

        private void btnListaAlumnos_Click(object sender, EventArgs e)
        {
            InscripcionModal inscripcionModal = new InscripcionModal(_grupo);
            inscripcionModal.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GruposModal_Load(object sender, EventArgs e)
        {
            cmbCarrera.DataSource = _carrerasManejador.ObtenerCarreras().Tables[0].DefaultView;
            cmbCarrera.ValueMember = "idCarrera";
            cmbCarrera.DisplayMember = "nombreCarrera";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            BuildGrupo();
            if (ValidarNombreGrupo())
            {
                Guardar();
                this.Close();
            }
        }

        private void Guardar()
        {
            _gruposManejador.Guardar(_grupo);
        }

        private bool ValidarNombreGrupo()
        {
            var res = _gruposManejador.EsNombreValido(_grupo);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return res.Item1;
        }
    }
}
