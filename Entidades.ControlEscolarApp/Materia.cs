﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolarApp
{
    public class Materia
    {
        private string _idMateria;
        private string _nombreMateria;
        private string _materiaAnterior;
        private string _materiaSiguiente;
        public string IdMateria { get => _idMateria; set => _idMateria = value; }
        public string NombreMateria { get => _nombreMateria; set => _nombreMateria = value; }
        public string MateriaAnterior { get => _materiaAnterior; set => _materiaAnterior = value; }
        public string MateriaSiguiente { get => _materiaSiguiente; set => _materiaSiguiente = value; }
    }
}
